LIBS=-lcrypto

all: preload.so openssl_test

.INTERMEDIATE: preload_test.o openssl_test.o

clean:
	rm -f *.o *.so openssl_test preload_test

check: preload_test preload.so
	LD_PRELOAD=./preload.so MALLOC_DEBUG=1 NO_SECRET_MEM=1 ./preload_test

%.so: %.c
	gcc -g -shared -fPIC -o $@ $^

%: %.o
	gcc -g -o $@ $^ $(LIBS)

%.o: %.c
	gcc -g -c $^
