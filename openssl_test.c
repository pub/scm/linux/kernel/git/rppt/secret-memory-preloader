#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <openssl/crypto.h>

int main(int argc, const char *argv[])
{
	char *secure_ptr = OPENSSL_malloc(256);
	char *insecure_ptr = malloc(256);

	strcpy(secure_ptr, "this is a secret");
	strcpy(insecure_ptr, "another secret");

	printf("secure_ptr holds \"%s\"\n", secure_ptr);
	printf("insecure_ptr holds \"%s\"\n", insecure_ptr);

	pause();

	exit(0);
}
